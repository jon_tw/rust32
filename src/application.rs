use crate::plat_independent_util::math::Point;
use crate::win32::win32_bmp::LoadedBitmap;
use crate::win32::win32_engine::Win32Engine;

pub struct Application {
    win32: Win32Engine,
}

impl Application {
    pub fn new(app_name: &str) -> Self {
        Self {
            win32: Win32Engine::new(app_name),
        }
    }

    pub fn run(&mut self) {
        let bmp = LoadedBitmap::load_bmp("Assets/poo.bmp");
        loop {
            self.win32.window.handle_events();

            if !self.win32.window.is_open() {
                break;
            }

            self.win32.screen_buffer.clear_screen(0);

            bmp.draw_bmp(Point::new(25, 25), &self.win32.screen_buffer);

            self.win32.screen_buffer.render_buffer_to_screen(
                self.win32.window.get_device_context(),
                self.win32.get_wnd_width(),
                self.win32.get_wnd_height(),
            );

            self.win32.time_manager.tick();
        }
    }
}
