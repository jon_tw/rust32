pub fn _as_fractional_secs(dur: &std::time::Duration) -> f32 {
    (dur.as_secs() as f64 + f64::from(dur.subsec_nanos()) / 1_000_000_000.0) as f32
}

#[derive(Copy, Clone)] // so one doesn't have to impl copy and clone traits manually
pub struct Point<T> {
    pub x: T,
    pub y: T,
}

impl<T> Point<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

#[derive(Copy, Clone)]
pub struct V2f {
    pub x: f32,
    pub y: f32,
}

#[allow(dead_code)]
impl V2f {
    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }

    pub fn _make_negative(&mut self) {
        self.x = -self.x;
        self.y = -self.y;
    }

    pub fn _add(&mut self, a: V2f, b: V2f) {
        self.x = a.x + b.x;
        self.y = a.y + b.y;
    }

    pub fn _sub(&mut self, a: V2f, b: V2f) {
        self.x = a.x - b.x;
        self.y = a.y - b.y;
    }
}

#[derive(Copy, Clone)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

#[allow(dead_code)]
impl Color {
    pub fn new(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self { r, g, b, a }
    }
}

#[derive(Copy, Clone)]
pub struct Rect<T> {
    pub x: T,
    pub y: T,
    pub w: T,
    pub h: T,
}

#[allow(dead_code)]
impl<T> Rect<T> {
    pub fn new(x: T, y: T, w: T, h: T) -> Self {
        Self { x, y, w, h }
    }
}
