// Time manager only relies on rust library
// (though it uses different functions depending on operating system)
// was previously in win32_engine.rs but that
// file is only for win32 related data.

use std::time::Instant;

// Time instant tracts time since the program was opened
pub struct TimeManager {
    time: Instant,
    time_step: f32,
    last_frame_time: f32,
    target_fps: Option<f32>,
}

// NOTE: time manager operates in seconds as baseline
impl TimeManager {
    pub fn new(target_fps: Option<f32>) -> Self {
        Self {
            time: Instant::now(),
            time_step: 0.0,
            last_frame_time: 0.0,
            target_fps: target_fps,
        }
    }

    pub fn tick(&mut self) {
        // Get the seconds elapsed
        let time = self.get_time_elapsed();

        // the time step is how long the the frame took

        // delta time = (current_time - how long the last frame took)
        let time_step = time - self.last_frame_time;
        self.last_frame_time = time; // set last frame time to the time

        // fps = 1000.0 / delta time in miliseconds

        //NOTE: if this crashes
        /*
            std::thread::sleep closes with (exit code: 0xcfffffff) meaning that the
            appilcation is hanging or windows at least thinks it is.
        */
        if self.target_fps.is_some() {
            let desired_time_step = 1.0 / self.target_fps.unwrap();

            if time_step < desired_time_step {
                let sleep_time = desired_time_step - time_step;

                self.time_step = time_step + sleep_time; // Does this make sense?

                std::thread::sleep(std::time::Duration::from_secs_f32(sleep_time));
            }
        } else {
            self.time_step = time_step;
        }

        /*println!(
            "FPS: {}, DT: {}, Last frame time: {}",
            self._get_fps(),
            self.time_step,
            self.last_frame_time
        );*/
    }

    // time s
    pub fn _get_frame_seconds(&self) -> f32 {
        self.time_step
    }

    // timestep in miliseconds
    pub fn _get_frame_miliseconds(&self) -> f32 {
        self.time_step * 1000.0
    }

    // timestep in frames per second
    // gives the wrong fps when using sleep
    pub fn _get_fps(&self) -> f32 {
        // fps = 1000.0 / delta time in miliseconds
        1000.0 / self._get_frame_miliseconds()
    }

    pub fn get_time_step(&self) -> f32 {
        self.time_step
    }

    pub fn get_time_elapsed(&self) -> f32 {
        // Returns time last since initialization
        self.time.elapsed().as_secs_f32()
    }
}
