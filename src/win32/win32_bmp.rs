use kernel32::*;
use std::ffi::CString;
use winapi::shared::basetsd::LONG_PTR;
use winapi::shared::minwindef::DWORD;
use winapi::um::winnt::*;

use super::win32_screenbuffer::Win32ScreenBuffer;
use crate::plat_independent_util::math::Point;

pub const INVALID_HANDLE_VALUE: HANDLE = (-1 as LONG_PTR) as HANDLE;
pub const OPEN_EXISTING: DWORD = 3;

// BitmapHeader struct from windows
#[repr(packed)]
pub struct BitmapHeader {
    pub file_type: u16,
    pub file_size: u32,
    pub reserved_1: u16,
    pub reserved_2: u16,
    pub bitmap_offset: u32,
    pub size: u32,
    pub width: i32,
    pub height: i32,
    pub planes: u16,
    pub bits_per_pixel: u16,
    pub compression: u32,
    pub size_of_bitmap: u32,
    pub horz_resolution: i32,
    pub vert_resolution: i32,
    pub colors_used: u32,
    pub colors_important: u32,
    pub red_mask: u32,
    pub green_mask: u32,
    pub blue_mask: u32,
}

// General win32 reading
pub struct ReadResult<'a> {
    pub contents: Option<&'a BitmapHeader>,
    pub raw_contents: *mut BitmapHeader,
    pub size: u64,
}

pub fn os_read_entire_file(file_path: &str) -> ReadResult {
    let mut result = ReadResult {
        contents: None,
        raw_contents: std::ptr::null_mut(),
        size: 0,
    };

    let file_path_converted = CString::new(file_path).expect("Failed to convert.");

    unsafe {
        // Get file
        let file_handle = CreateFileA(
            file_path_converted.as_ptr() as LPCSTR,
            GENERIC_READ,
            FILE_SHARE_READ,
            std::ptr::null_mut(),
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL,
            std::ptr::null_mut(),
        );

        // If the file handle is -1 there's a problem
        assert!(
            file_handle != INVALID_HANDLE_VALUE as *mut std::ffi::c_void,
            "File handle wrong."
        );

        // Read file size
        let file_size = GetFileSize(file_handle as *mut std::ffi::c_void, std::ptr::null_mut());

        result.size = file_size as u64;

        let content_read =
            HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, file_size as u64) as *mut BitmapHeader;
        result.contents = Some(&*content_read); // I don't think I use this, only the raw but :shrug:
        result.raw_contents = content_read;

        //TODO: Fix this readfile check
        //NOTE: 12/1/21 does this still ened to be fixed?
        let bytes_read: *mut u32 = 0 as *mut u32;

        let file_read = ReadFile(
            file_handle as *mut std::ffi::c_void,
            content_read as *mut std::ffi::c_void,
            file_size,
            bytes_read,
            std::ptr::null_mut(),
        );

        // Possibly change if checks for asserts - though not too worried about
        // such a simple thing atm.
        assert!(
            file_read != 0,
            "Failed to read file. Function: os_read_entire_file()"
        );

        CloseHandle(file_handle);
    }

    result
}

// A bmp texture
#[derive(Clone, Copy)]
pub struct LoadedBitmap {
    pub pixels: *mut u32,
    pub width: u32,
    pub height: u32,
}

impl LoadedBitmap {
    // These functions and methods are meant for BMP Textures
    pub fn load_bmp(file_path: &str) -> LoadedBitmap {
        // Declare return variable
        let mut result = {
            LoadedBitmap {
                pixels: std::ptr::null_mut(),
                width: 0,
                height: 0,
            }
        };

        // Read the file passed by argument
        let file_read = os_read_entire_file(file_path);

        // Set header as read content
        let header = file_read.contents;

        // I think is is now correct.
        let raw = file_read.raw_contents as *mut u8;
        let pixels =
            unsafe { raw.add(header.as_ref().unwrap().bitmap_offset as usize) } as *mut u32;

        // Set the results
        result.pixels = pixels;
        result.width = header.unwrap().width as u32;
        result.height = header.unwrap().height as u32;

        println!(
            "Texture Width: {} & Height: {}",
            result.width, result.height
        );

        result
    }

    // Render entire image file
    pub fn draw_bmp(&self, pos: Point<u32>, buffer: &Win32ScreenBuffer) {
        unsafe {
            let source_pixels = self.pixels;
            let mut source_row = source_pixels.add((self.width * (self.height - 1)) as usize);

            let dest_pixels = buffer.memory as *mut u32;
            let mut dest_row =
                dest_pixels.add((pos.y * buffer.screen_width as u32 + pos.x) as usize);

            for _y in 0..self.height {
                let mut dest = dest_row;
                let mut source = source_row;

                for _x in 0..self.width {
                    // Only render if the alpha channel is > 128
                    if (*source >> 24) > 128 {
                        *dest = *source;
                    }

                    //*dest += 1;
                    dest = dest.add(1);
                    //*source += 1;
                    source = source.add(1);
                }

                //*dest_row += engine.get_width();
                //*source_row -= self.width;
                let width = self.width as i32;
                dest_row = dest_row.add(buffer.screen_width as usize);
                source_row = source_row.add(-width as usize);
            }
        }
    }
}
