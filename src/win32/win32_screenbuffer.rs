/*
 * Win32 screen buffer module.
 *
*/

use kernel32::*;
use winapi::shared::windef::*;
use winapi::um::wingdi::*;
use winapi::um::winnt::*;

pub struct Win32ScreenBuffer {
    pub bitmap_info: BITMAPINFO,
    pub memory: *const winapi::ctypes::c_void,
    pub screen_width: i32,  // Current sizes
    pub screen_height: i32, //
}

impl Win32ScreenBuffer {
    pub fn new(window: &HWND, screen_width: i32, screen_height: i32) -> Self {
        // Buffer struct initialization
        let buffer: BITMAPINFO = BITMAPINFO {
            bmiHeader: BITMAPINFOHEADER {
                biSize: std::mem::size_of::<BITMAPINFOHEADER>() as u32,
                biWidth: screen_width,
                biHeight: -screen_height, // negative so it renders from the top left
                biPlanes: 1,
                biBitCount: 32,
                biCompression: BI_RGB,
                biSizeImage: 0,
                biXPelsPerMeter: 0,
                biYPelsPerMeter: 0,
                biClrUsed: 0,
                biClrImportant: 0,
            },
            bmiColors: [RGBQUAD {
                // Array with one struct
                rgbBlue: 0,
                rgbGreen: 0,
                rgbRed: 0,
                rgbReserved: 0,
            }],
        };

        // Buffer and Memory declaration and assignment
        let height = buffer.bmiHeader.biHeight * -1; // Make height positive

        // Calc buffer size: width * height * sizeof(u32)
        let buffer_size = buffer.bmiHeader.biWidth * height * std::mem::size_of::<u32>() as i32;

        // Declare buffer & set to 0
        let mut _memory = 0 as *const winapi::ctypes::c_void;

        // Fill the buffer
        unsafe {
            // If the buffer is not null, clear it
            if !_memory.is_null() {
                VirtualFree(_memory as *mut std::ffi::c_void, 0, MEM_RELEASE);
            }

            // Allocate the memory for the buffer
            _memory = VirtualAlloc(
                std::ptr::null_mut(),
                buffer_size as u64,
                MEM_COMMIT | MEM_RESERVE,
                PAGE_READWRITE,
            ) as *const winapi::ctypes::c_void;
        }

        Self {
            bitmap_info: buffer,
            memory: _memory,
            screen_width,
            screen_height,
        }
    }

    pub fn resize_buffer(&mut self, screen_width: i32, screen_height: i32) {
        let buffer_size = screen_width * screen_height * std::mem::size_of::<u32>() as i32;

        unsafe {
            if !self.memory.is_null() {
                VirtualFree(self.memory as *mut std::ffi::c_void, 0, MEM_RELEASE);
            }

            // Refill the buffer info
            self.bitmap_info = BITMAPINFO {
                bmiHeader: BITMAPINFOHEADER {
                    biSize: std::mem::size_of::<BITMAPINFOHEADER>() as u32,
                    biWidth: screen_width,
                    biHeight: -screen_height,
                    biPlanes: 1,
                    biBitCount: 32,
                    biCompression: BI_RGB,
                    biSizeImage: 0,
                    biXPelsPerMeter: 0,
                    biYPelsPerMeter: 0,
                    biClrUsed: 0,
                    biClrImportant: 0,
                },
                bmiColors: [RGBQUAD {
                    rgbBlue: 0,
                    rgbGreen: 0,
                    rgbRed: 0,
                    rgbReserved: 0,
                }],
            };

            // Re-allocate the buffer - allocate the new buffer size
            self.memory = VirtualAlloc(
                std::ptr::null_mut(),
                buffer_size as u64,
                MEM_COMMIT | MEM_RESERVE,
                PAGE_READWRITE,
            ) as *const winapi::ctypes::c_void;
        }
    }

    pub fn clear_screen(&self, color: u32) {
        let height = self.bitmap_info.bmiHeader.biHeight * -1;
        unsafe {
            libc::memset(
                self.memory as *mut libc::c_void,
                color as i32,
                (self.bitmap_info.bmiHeader.biWidth * height * 4) as usize,
            );
        }
    }

    pub fn render_buffer_to_screen(
        &mut self,
        device_context: HDC,
        screen_width: i32,
        screen_height: i32,
    ) {
        // If the parameter screen size is different from the current screen size,
        // set new screen size and resize the buffer
        if self.screen_width != screen_width || self.screen_height != screen_height {
            // Set new render res
            self.screen_width = screen_width;
            self.screen_height = screen_height;

            // Resize the buffer
            self.resize_buffer(self.screen_width, self.screen_height);
        }

        unsafe {
            StretchDIBits(
                device_context,
                0,
                0,
                self.screen_width,
                self.screen_height,
                0,
                0,
                self.screen_width,
                self.screen_height,
                self.memory,
                &self.bitmap_info,
                DIB_RGB_COLORS,
                SRCCOPY,
            );
        }
    }
}
