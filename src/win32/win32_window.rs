/*
 * Win32 window creation module.
 * Just using standard windows functions to create a window along with utility methods.
*/

use std::ffi::CString;
use std::ffi::OsStr;
use std::os::windows::ffi::OsStrExt;

// Windows' stuff:
use kernel32::*;
use libc::exit;
use winapi::shared::minwindef::*;
use winapi::shared::ntdef::*;
use winapi::shared::windef::*;
use winapi::shared::winerror::*;
use winapi::um::wingdi::*;
use winapi::um::winuser::*;

// Wide char array C/C++ ex. == WCHAR *string = L"String";
pub fn create_wide_char(string: &str) -> Vec<u16> {
    let mut result: Vec<u16> = OsStr::new(string).encode_wide().collect();

    result.push(0); // add null terminator

    result
} // using function with .as_ptr() method ex. create_wide_char("Monke Game").as_ptr();

static mut IS_WINDOW_CLOSED: bool = false; // Only for the window proc function below

pub enum WindowMessages {
    WindowClosed,
}

unsafe extern "system" fn window_proc(
    hwnd: HWND,
    msg: UINT,
    w_param: WPARAM,
    l_param: LPARAM,
) -> LRESULT {
    if msg == WM_CLOSE {
        IS_WINDOW_CLOSED = true;
        PostQuitMessage(0);
    }

    DefWindowProcW(hwnd, msg, w_param, l_param)
}

/* Utility structure for window. */

// Storage for Screen data that excludes the windows bar
pub struct ClientData {
    pub width: i32,
    pub height: i32,
}

#[derive(Clone, Copy)]
pub struct Win32Window {
    hwnd: HWND, // Window itself
    device_context: HDC,
    is_open: bool, // Whether window is open or not
}

impl Win32Window {
    pub fn new(window_name: &str) -> Self {
        unsafe {
            // Convert window name to win32 usable string
            let app_name = create_wide_char(window_name);

            // Before creating the window check if the same application
            // is already open or not

            let mutex_name = CString::new("__WindowMutex").expect("Failed");

            // Mutex -- basically the thing that will be checked to see if app already open
            let _mutex = CreateMutexA(std::ptr::null_mut(), 0, mutex_name.as_ptr());

            let error_message =
                CString::new("Another instance of this program as already open.").expect("Failed");
            let error = CString::new("Error!").expect("Failed");

            // If the application already exists, show message box and exit
            if GetLastError() == ERROR_ALREADY_EXISTS {
                MessageBoxA(
                    std::ptr::null_mut(),
                    error_message.as_ptr(),
                    error.as_ptr(),
                    MB_ICONEXCLAMATION | MB_OK,
                );
                exit(1);
            }

            // Create window class
            let window_class = WNDCLASSW {
                style: 0,
                lpfnWndProc: Some(window_proc),
                cbClsExtra: 0,
                cbWndExtra: 0,
                hInstance: 0 as HINSTANCE,
                hIcon: 0 as HICON,
                hCursor: 0 as HICON,
                hbrBackground: 16 as HBRUSH,
                lpszMenuName: 0 as LPCWSTR,
                lpszClassName: create_wide_char("MyWindowClass").as_ptr(),
            };

            // Check if window class creation worked
            let error_code = RegisterClassW(&window_class);
            assert!(error_code != 0, "Failed to register the window class.");

            // Create window
            let window = CreateWindowExW(
                0,
                create_wide_char("MyWindowClass").as_ptr(),
                app_name.as_ptr(),
                WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                CW_USEDEFAULT,
                0 as HWND,
                0 as HMENU,
                0 as HINSTANCE,
                std::ptr::null_mut(),
            );

            // Check if window created worked
            assert!(window != (0 as HWND), "Failed to open the window.");

            // Show window so it's not invisible and then update the window
            ShowWindow(window, SW_SHOW);
            UpdateWindow(window);

            // way to hide console
            let console_window = GetConsoleWindow();
            ShowWindow(console_window as *mut HWND__, SW_HIDE);

            Self {
                hwnd: window,
                device_context: GetDC(window),
                is_open: true,
            }
        }
    }

    pub fn process_window_messages(&self) -> Option<WindowMessages> {
        unsafe {
            let mut msg: MSG = std::mem::zeroed();

            // Process messages
            while PeekMessageA(&mut msg, self.hwnd, 0, 0, PM_REMOVE) > 0 {
                TranslateMessage(&msg);
                DispatchMessageA(&msg);

                if IS_WINDOW_CLOSED {
                    return Some(WindowMessages::WindowClosed);
                }
            }

            None
        }
    }

    pub fn handle_events(&mut self) {
        while let Some(x) = self.process_window_messages() {
            match x {
                WindowMessages::WindowClosed => {
                    self.is_open = false;
                    break; // Don't run the rest of the loop
                }
            }
        }
    }

    pub fn get_refresh_rate(&self) -> i32 {
        return unsafe { GetDeviceCaps(self.device_context, VREFRESH) };
    }

    pub fn get_hwnd(&self) -> &HWND {
        &self.hwnd
    }

    pub fn get_device_context(&self) -> HDC {
        self.device_context
    }

    pub fn get_size(&self) -> ClientData {
        let mut result = ClientData {
            width: 0,
            height: 0,
        };

        let mut rect = RECT {
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
        };

        unsafe {
            GetClientRect(self.hwnd, &mut rect);
        }

        result.width = rect.right - rect.left;
        result.height = rect.bottom - rect.top;

        result
    }

    pub fn is_open(&self) -> bool {
        self.is_open
    }

    pub fn close_window(&mut self) {
        self.is_open = false;
    }
}
