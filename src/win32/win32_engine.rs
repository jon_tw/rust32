/*
 * Just includes all window modules into one making an "engine" in order to fully
 * have a functioning application: sound, window, file loading, bitmaps, etc.. in
 * one structure.
 * Created: 3/17/22
 */

use super::win32_screenbuffer::Win32ScreenBuffer;
use super::win32_window::Win32Window;
use crate::plat_independent_util::time_manager::TimeManager;

pub struct Win32Engine {
    pub window: Win32Window,
    pub screen_buffer: Win32ScreenBuffer,
    pub time_manager: TimeManager,
}

impl Win32Engine {
    pub fn new(window_name: &str) -> Self {
        let window = Win32Window::new(window_name);
        let window_size = window.get_size();

        Self {
            // Create window
            window,
            // Create and fill screen buffer
            screen_buffer: Win32ScreenBuffer::new(
                window.get_hwnd(),
                window_size.width,
                window_size.height,
            ),
            // Create time manager (manages fps)
            time_manager: TimeManager::new(Some(window.get_refresh_rate() as f32)),
            // Create sound buffer

            // Create controller input

            // Load files??
        }
    }

    pub fn get_wnd_width(&self) -> i32 {
        self.window.get_size().width
    }

    pub fn get_wnd_height(&self) -> i32 {
        self.window.get_size().height
    }
}
